/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanalogin;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Hugo
 */
public class ventana2 extends JFrame{
    private Container contenedor2;
    private JButton salir;
    private JLabel etiqueta;
    
    public ventana2(){
        super();
        setVisible(true);
        setSize(500,400);
        setTitle("Ventana2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contenedor2=(JPanel) getContentPane();
        contenedor2.setLayout(null);
        salir=new JButton("Salir");
        contenedor2.add(salir);
        salir.setBounds(175, 250, 150, 30); 
        etiqueta=new JLabel("ESTÁS LOGEADO");
        contenedor2.add(etiqueta);
        etiqueta.setBounds(180, 100, 150, 50);
        
        salir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {  
                if (e.getSource()==salir) {
                try{
// Que se cierre la ventana                    
                    dispose();
                    Ventana v2=new Ventana();
                } catch(Exception excep) {
                    System.out.println("Ha ocurrido un error");
                    dispose();
                }
            }
            }
        });
    }
        
}
