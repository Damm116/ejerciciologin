/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanalogin;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

/**
 *
 * @author damm116
 */
public class Ventana extends JFrame {
    private Container contenedor;
    private JButton boton;
    private JTextArea textUser;
    private JTextArea textPassword;
    private JLabel etiquetaUser;
    private JLabel etiquetaPassword;
    
    public Ventana(){
        // Llamamos al constructor de la superclase        
        super();
// Para que aparezca la ventana        
        setVisible(true);
// Dimensiones para la ventana(ancho/alto)        
        setSize(500,400);
// Título de la ventana        
        setTitle("Ventana1");
// No dejar que el tamaño de la ventana cambie        
//        setResizable(false);
// Localización de la ventana (cambiar la posición)
        setLocation(100,100);
// Para matar el proceso desde la X de la ventana y no desde el propio netBeans
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
// getContentPane me devuelve el panel de la ventana        
        contenedor=(JPanel) getContentPane();
// Definimos el comportamiento de layout(diseño)        
        contenedor.setLayout(null);
// Botón
        boton=new JButton("Aceptar");
//Añadir al contenedor
        contenedor.add(boton);
// Posición
        boton.setBounds(175, 250, 150, 30);
// ETIQUETAS
        etiquetaUser=new JLabel("Usuario: ");
        etiquetaPassword=new JLabel("Password: ");
// Añadir al contenedor        
        contenedor.add(etiquetaUser);
        contenedor.add(etiquetaPassword);
// Setbounds
        etiquetaUser.setBounds(40, 100, 100, 50);
        etiquetaPassword.setBounds(40,150,100,50);
//TEXTAREA
        textUser=new JTextArea();
        textPassword=new JTextArea();
        contenedor.add(textUser);
        contenedor.add(textPassword);
        textUser.setBounds(120,115,150,20);
        textPassword.setBounds(120,165,150,20);
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               boolean validacion=false;
               if(textPassword.getText().matches("^[A-Z]{1}[A-Za-z]{3,14}$")){
                   validacion=true;
               }
                if (e.getSource()==boton) {
                try{
                    //Meter las validaciones con el match y boolean
                    
                      if(textPassword.getText().length()>8 && validacion){              
                        ventana2 v2=new ventana2();
                        dispose();
                      }else{                   
                          JOptionPane.showMessageDialog(contenedor, "Usuario y contraseña incorrectos ", "Error", JOptionPane.ERROR_MESSAGE);
                      }          
                } catch(Exception excep) {
                    System.exit(0);
                }
            }
            }
        });
    } 
}
